import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
const Lists = (props)=>{
    
    return (
            <ListItem>
                    <ListItemIcon>
                        <IconButton onClick={()=>{props.onSelect(props.id)}} color="secondary" aria-label="Delete" component="span">
                            <DeleteIcon/>
                        </IconButton>
                    </ListItemIcon>
                    <ListItemText primary={props.text} />
            </ListItem>
    )
}
export default Lists;