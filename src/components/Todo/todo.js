import React, { useState } from 'react';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import Card from '@material-ui/core/Card';
import Lists from './todolist';
import { red } from '@material-ui/core/colors';

function Todo() {
    const [inputList,setInputList] = useState("");
    const [err,setErr] = useState();
    const [Items, setItems] = useState([]);
    const itemEvent = (event)=>{
        setInputList(event.target.value)
    }
    const listOfItems = ()=>{
        if(inputList === ""){
            setErr("Please add smthng");
        }else if(inputList.length < 6){
            setErr("Minimum length is 6")
        }else{
        setItems((oldItems)=>{
            return [...oldItems, inputList]
        })
        setInputList('')
        setErr("")
    } 
    }
    const deleteItems = (id)=>{
        console.log('clicked')
        setItems((oldItems)=>{
            return oldItems.filter((arrayElem,index)=>{
                return index !== id;
            })
        })}
    return (
      <>
        
        <Container maxWidth="sm">
            <TextField id="standard-basic"  label="Enter Todo" value={inputList} onChange={itemEvent} />
            <IconButton color="primary" onClick={listOfItems}  aria-label="Add a items"  component="span">
                <AddCircleIcon/>
            </IconButton>
            <div style={{color:"red"}}>{err}</div>
            <br/><br/>
            <Card>
            <List>
                {Items.map((itemval, index) => {
                   return <Lists key={index} id={index} text={itemval} onSelect={deleteItems}/>
                })}
            </List>
            </Card>
        </Container>
        
      </>
    );
  }
  
  export default Todo;