import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';

function Header() {
    return (
      <>
      <header>
      <AppBar position="static">
      <Typography variant="h2" gutterBottom style={{textAlign:"center"}}>
          Todo List
      </Typography>
      </AppBar>
      </header>
      <br/><br/>
      </>
    );
  }
  
  export default Header;