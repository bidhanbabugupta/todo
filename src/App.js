import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from "./components/Header/header";
import Todo from "./components/Todo/todo";

function App() {
  return (
    <>
    <Header/>
    <Todo/>
    </>
  );
}

export default App;
